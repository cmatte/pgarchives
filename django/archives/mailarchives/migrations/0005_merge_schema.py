from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mailarchives', '0004_resend_rate_limit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='parentid',
            field=models.ForeignKey(db_column='parentid', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.Message', null=True),
        ),
        migrations.AddField(
            model_name='message',
            name='rawtxt',
            field=models.BinaryField(blank=True, null=False),
        ),
        migrations.AddField(
            model_name='attachment',
            name='attachment',
            field=models.BinaryField(blank=True, null=False),
        ),
        migrations.CreateModel(
            name='Loaderrors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dat', models.DateTimeField(auto_now_add=True)),
                ('msgid', models.TextField()),
                ('srctype', models.TextField()),
                ('src', models.TextField()),
                ('err', models.TextField()),
                ('listid', models.ForeignKey(db_column='listid', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.List')),
            ],
            options={
                'db_table': 'loaderrors',
            },
        ),
        migrations.CreateModel(
            name='UnresolvedMessages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.IntegerField()),
                ('msgid', models.TextField()),
                ('message', models.ForeignKey(db_column='message', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.Message')),
            ],
            options={
                'db_table': 'unresolved_messages',
                'unique_together': {('message', 'priority', 'msgid')},
            },
        ),
        migrations.CreateModel(
            name='ListThreads',
            fields=[
                ('threadid', models.IntegerField(serialize=False)),
                ('listid', models.ForeignKey(db_column='listid', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.List')),
            ],
            options={
                'db_table': 'list_threads',
            },
        ),
        migrations.CreateModel(
            name='ListMonths',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField()),
                ('month', models.IntegerField()),
                ('listid', models.ForeignKey(db_column='listid', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.List')),
            ],
            options={
                'db_table': 'list_months',
                'unique_together': {('listid', 'year', 'month')},
            },
        ),
        migrations.CreateModel(
            name='Legacymap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField()),
                ('month', models.IntegerField()),
                ('msgnum', models.IntegerField()),
                ('msgid', models.TextField()),
                ('listid', models.ForeignKey(db_column='listid', on_delete=django.db.models.deletion.CASCADE, to='mailarchives.List')),
            ],
            options={
                'db_table': 'legacymap',
                'unique_together': {('listid', 'year', 'month', 'msgnum')},
            },
        ),
        migrations.RunSQL("ALTER TABLE messages ADD COLUMN fti tsvector NOT NULL"),
        migrations.RunSQL("CREATE INDEX idx_messages_threadid ON messages(threadid);"),
        migrations.RunSQL("CREATE UNIQUE INDEX idx_messages_msgid ON messages(messageid);"),
        migrations.RunSQL("CREATE INDEX idx_messages_date ON messages(date);"),
        migrations.RunSQL("CREATE INDEX idx_messages_parentid ON messages(parentid);"),
        migrations.RunSQL("CREATE SEQUENCE threadid_seq;"),
        migrations.RunSQL("CREATE UNIQUE INDEX idx_unresolved_msgid_message ON unresolved_messages(msgid, message);"),
        migrations.RunSQL("CREATE INDEX idx_attachments_msg ON attachments(message);"),
        migrations.RunSQL("ALTER TABLE loaderrors ALTER COLUMN dat SET DEFAULT NOW();"),
        migrations.RunSQL("ALTER TABLE list_threads DROP CONSTRAINT list_threads_pkey;"),
        migrations.RunSQL("ALTER TABLE list_threads ADD CONSTRAINT pg_list_threads PRIMARY KEY (threadid, listid);"),
        migrations.RunSQL("""
CREATE TEXT SEARCH CONFIGURATION pg (COPY = pg_catalog.english);

CREATE TEXT SEARCH DICTIONARY english_ispell (
   TEMPLATE = ispell,
   DictFile = en_us,
   AffFile = en_us,
   StopWords = english
);
CREATE TEXT SEARCH DICTIONARY pg_dict (
   TEMPLATE = synonym,
   SYNONYMS = pg_dict
);
CREATE TEXT SEARCH DICTIONARY pg_stop (
   TEMPLATE = simple,
   StopWords = pg_dict
);
ALTER TEXT SEARCH CONFIGURATION pg
   ALTER MAPPING FOR asciiword, asciihword, hword_asciipart,
                     word, hword, hword_part
    WITH pg_stop, pg_dict, english_ispell, english_stem;
ALTER TEXT SEARCH CONFIGURATION pg
   DROP MAPPING FOR email, url, url_path, sfloat, float;

CREATE FUNCTION messages_fti_trigger_func() RETURNS trigger AS $$
BEGIN
   NEW.fti = setweight(to_tsvector('public.pg', coalesce(new.subject, '')), 'A') ||
             setweight(to_tsvector('public.pg', coalesce(new.bodytxt, '')), 'D');
   RETURN NEW;
END

$$ LANGUAGE 'plpgsql';

CREATE TRIGGER messages_fti_trigger
 BEFORE INSERT OR UPDATE OF subject, bodytxt ON  messages
 FOR EACH ROW EXECUTE PROCEDURE messages_fti_trigger_func();
CREATE INDEX messages_fti_idx ON messages USING gin(fti);

/* Simple API for hiding messages */
CREATE OR REPLACE FUNCTION hide_message(msgid_txt text, reason_code integer, user_txt text, reason_txt text)
  RETURNS integer AS
$BODY$
DECLARE
    returned_id integer;
BEGIN
    UPDATE messages SET hiddenstatus = reason_code WHERE messageid = msgid_txt RETURNING id INTO returned_id;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'The specified message (%) could not be found.', msgid_txt;
    END IF;

    INSERT INTO message_hide_reasons (message, dt, reason, by) VALUES (returned_id, now(), reason_txt, user_txt);

    RETURN returned_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
""")
    ]
