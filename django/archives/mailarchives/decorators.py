import datetime
from functools import wraps
from collections import defaultdict
from django.contrib.auth.decorators import login_required as django_login_required


def queryparams(*args):
    """
    Allow specified query parameters when calling function.
    NOTE! Must be the "outermost" decorator!!!
    """
    def _queryparams(fn):
        fn.queryparams = args
        return fn
    return _queryparams
