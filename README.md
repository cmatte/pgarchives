# PGArchives

PGArchives is a mailing list archiving software designed to be used for the mailing list software [PGLister](https://gitlab.com/pglister/pglister).

# Install

The complete install procedure for pglister and its component is described [here](https://gitlab.com/cmatte/pglister/-/blob/master/INSTALL.md).

The easiest way it to use [the ansible script](https://gitlab.com/cmatte/ansible-pglister).

